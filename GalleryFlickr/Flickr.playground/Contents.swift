// requete sur notre api (GetSandbox)

import PlaygroundSupport
import UIKit

let queue = DispatchQueue(label: "com.maboite.Flickr", qos: .background, attributes: .concurrent)

var id = String()
var secret = String()
var server = String()
var farm = String()
var title = String()

var urlFlickr: [String] = []

let url = URL(string:"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=c10755ff8b0576552f75eeb3bbaa1bb7&tags=bikini&format=json&nojsoncallback=1&api_sig=f6589b82b868b7070483788f3b3e62fd")

let session = URLSession.shared

queue.async {
    let getUserstask = session.dataTask(with: url!) {
        (data, response, error) in
        
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let data = data else {
            print("Pas de donnees")
            return
        }
        
        let allPhotosData = try Data(contentsOf: url!)
        let allPhotos = try JSONSerialization.jsonObject(with: allPhotosData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
        if let arrJSON = allPhotos["photos"]?["photo"] as? [[String: Any]]{
            for index in 0...arrJSON.count-1 {
                
                let aObject = arrJSON[index]
                
                id = (aObject["id"] as! String)
                secret = (aObject["secret"] as! String)
                server = (aObject["server"] as! String)
                farm = ((aObject["farm"] as! Int).description)
                title = (aObject["title"] as! String)
                
                urlFlickr.append("https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_m.jpg")
            }
        }
        
        print(urlFlickr)

        /*
        if let flickr = flickr {
            print(flickr)
        }
        */
        print("ok")
    }
}

/*
do {
    let allPhotosData = try Data(contentsOf: url!)
    let allPhotos = try JSONSerialization.jsonObject(with: allPhotosData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
    if let arrJSON = allPhotos["photos"]?["photo"] as? [[String: Any]]{
        for index in 0...arrJSON.count-1 {
            
            let aObject = arrJSON[index]
            
            id = (aObject["id"] as! String)
            secret = (aObject["secret"] as! String)
            server = (aObject["server"] as! String)
            farm = ((aObject["farm"] as! Int).description)
            title = (aObject["title"] as! String)
            
            urlFlickr.append("https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_m.jpg")
        }
    }
    
    print(urlFlickr)
    
}
catch {
    
}
*/






// Pour ne pas interrompre le background et recevoir la réponse
PlaygroundPage.current.needsIndefiniteExecution = true
