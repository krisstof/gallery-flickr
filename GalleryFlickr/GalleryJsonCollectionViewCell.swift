//
//  GalleryJsonCollectionViewCell.swift
//  GalleryFlickr
//
//  Created by Apple on 27/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class GalleryJsonCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelText: UILabel!
    
    
}
