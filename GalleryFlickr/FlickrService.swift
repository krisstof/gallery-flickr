//
//  FlickrService.swift
//  GalleryFlickr
//
//  Created by Apple on 24/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import CoreData


class FlickrService: GalleryJsonCollectionViewController {
    
    //https://www.flickr.com/services/api/explore/flickr.photos.search
    let apiKey = ""
    
    var id = String()
    var secret = String()
    var server = String()
    var farm = String()
    //var titre = String()
    var urlFlickr: [String] = []
    var titre: [String] = []
    

    
    var tbTitreUrl: [[String]] = [[]]
    
    var locPicture: [String] = []
    var idLoc = String()
    var longitude = Double()
    var latitude = Double()
    var urlFlickrLoc: [[String:Double]] = []
    
    var photosLocalization: [[String : Double]] = []
    
    var session = URLSession.shared
    
    
    // Mon singleton
    static let sharedInstance: FlickrService = FlickrService()
    
        
    // completionHandler = callback
    func searchFlickr(tag:String, text:String, max_taken_date:String, _ completionHandler: @escaping ([String]) -> Void) {
        
        // Ma closure
        let json = session.dataTask(with: jsonParse(tag: tag, text: text, max_taken_date: max_taken_date)) {
            data, response, error in
            
            guard error == nil else {
                print("[URLDataTask error]: \(error?.localizedDescription)")
                return
            }
            
            guard let data = data else {
                print("[URLDataTask no data]")
                return
            }
            
            guard let allPhotos = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject] else {
                print("[URLDataTask completion]: error serialization...")
                return
            }
            
            self.urlFlickr = []
            
            if let arrJSON = allPhotos["photos"]?["photo"] as? [[String: Any]] {
                for index in 0...arrJSON.count-1 {
                    
                    let aObject = arrJSON[index]
                    self.id = (aObject["id"] as! String)
                    self.secret = (aObject["secret"] as! String)
                    self.server = (aObject["server"] as! String)
                    self.farm = ((aObject["farm"] as! Int).description)
                    //self.titre = (aObject["title"] as! String)
                    self.titre.append(aObject["title"] as! String)
                    
                    self.urlFlickr.append("https://farm\(self.farm).staticflickr.com/\(self.server)/\(self.id)_\(self.secret).jpg")
                    //self.tbTitreUrl[index].append(self.titre)
                    //self.tbTitreUrl[index].append(self.id)
                    
                    self.searchFlickrLocalization(id: self.id, { (localisation) in
                        self.photosLocalization = localisation
                    })
                    
                    //print(self.photosLocalization)
                    //print(self.tbTitreUrl)
                }
                
            }
            //print(self.titre)
            // On renvoie le callback
            completionHandler(self.urlFlickr)
            //completionHandler(self.tbTitreUrl as! [[String]] )
        }
        
        json.resume()
        
    }
    
    private func jsonParse(tag: String, text: String, max_taken_date: String) -> URL {
        let requestURL = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)\(tag)\(text)\(max_taken_date)&per_page=500&format=json&nojsoncallback=1"
        return URL(string: requestURL)!
    }
    
    func addPictureFavoris(index: Int) -> String {
        
        return urlFlickr[index]+"@"+titre[index]
        
    }
    
    
    
    func searchFlickrLocalization(id: String, _ completionHandler: @escaping ([[String:Double]]) -> Void) {
        
        // Ma closure
        let jsonLoc = session.dataTask(with: jsonParseLoc(id: id)) {
            data, response, error in
            
            guard error == nil else {
                print("[URLDataTask error]: \(error?.localizedDescription)")
                return
            }
            
            guard let data = data else {
                print("[URLDataTask no data]")
                return
            }
            
            guard let allPhotos = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject] else {
                print("[URLDataTask completion]: error serialization...")
                return
            }
            
            
            if(allPhotos["stat"] as! String == "ok") {
               
                //print(allPhotos["photo"]?["location"]!)
                if let arrJSON = allPhotos["photo"] as? [String: Any],
                let allPhoto = arrJSON["location"] as? [String: Any]{
                  //print(allPhoto["latitude"])
                    self.latitude = Double(allPhoto["latitude"] as! String)!
                    self.longitude = Double(allPhoto["longitude"] as! String)!
                 
                    //self.urlFlickrLoc.append("\(self.latitude)@\(self.longitude)")
                    //self.urlFlickrLoc.updateValue(self.latitude, forKey: "latitude")
                    self.urlFlickrLoc.append(["latitude":self.latitude,"longitude":self.longitude])
                    
                }
            }
            
            //print(self.urlFlickrLoc)
            completionHandler(self.urlFlickrLoc)
            
        }
        
        jsonLoc.resume()
        
    }
    
    //33923966946
    func jsonParseLoc(id: String) -> URL {
        let requestURL = "https://api.flickr.com/services/rest/?method=flickr.photos.geo.getLocation&api_key=c0f82098a57b5dde27befffb8ed75e66&photo_id=\(id)&format=json&nojsoncallback=1"
        return URL(string: requestURL)!
    }
    
    
}

