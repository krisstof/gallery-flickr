//
//  FavorisCollectionViewCell.swift
//  GalleryFlickr
//
//  Created by Apple on 06/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class FavorisCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var labelFavoris: UILabel!
    
}
