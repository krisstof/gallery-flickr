//
//  GalleryJsonCollectionViewController.swift
//  GalleryFlickr
//
//  Created by Apple on 27/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "image-cell"

class GalleryJsonCollectionViewController: UICollectionViewController {
    
    var flickr = String()
    var flickrTags = String()
    var flickrText = String()
    var flickrTaken = String()
    
    var tag = String()
    var text = String()
    var max_taken_date = String()
    
    var pictureUrl: [String] = []
    var pictureLabel: [String] = []
    
    var ttm = String()
    
    var api_key = String()
    var photosSearch: [String] = []
    
    
    @IBOutlet weak var labelApi: UILabel!
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Reception de l'api_key dans le fichier plist
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            api_key = dict["API_KEY"] as! String
            //print(api_key)
        }
        
        if(flickrTags != "") {
            flickr = flickrTags
            tag = "&tags="+flickrTags.replacingOccurrences(of: " ", with: "+")
        }else if(flickrText != "") {
            flickr = flickrText
            text = "&text="+flickrText
        }else if(flickrTaken != "") {
            flickr = flickrTaken
            max_taken_date = "&max_taken_date="+flickrTaken
        }else { flickr = "Error" }
        
        self.navigationItem.title = "Vignette Flickr (\(flickr))"

        // SharedInstance
        let myFlickrService = FlickrService.sharedInstance

        // Appel de la closure par une autre dans la fonction "photos"
        myFlickrService.searchFlickr(tag:tag, text:text, max_taken_date:max_taken_date) { (photos) in
            self.photosSearch = photos
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
            //print(self.photosSearch)
        }
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "image-detail-seque"{
            if let cell = sender as? GalleryJsonCollectionViewCell,
                let detailImageVC = segue.destination as? DetailFlickrViewController {
                
                detailImageVC.image = cell.imageCell.image
                detailImageVC.titre = cell.labelText.text!
                //print(cell.imageCell.image)
            }
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return photosSearch.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GalleryJsonCollectionViewCell
        
        let flickrUrl = URL(string: photosSearch[indexPath.row])
        cell.backgroundColor = UIColor.gray
        
        let task = URLSession.shared.dataTask(with: flickrUrl!) { (data, response, error) in
            DispatchQueue.main.async {
                cell.imageCell.image = UIImage(data: data!)
            }
        }
        
        task.resume()
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longTapFuncfavoris))
        cell.addGestureRecognizer(longPressRecognizer)
        
        return cell
        
    }

    
    func longTapFuncfavoris(sender: UILongPressGestureRecognizer) {
        
        // Recuperation de l'index de l'image
        let touchPoint = sender.location(in: collectionView)
        let indexPath = collectionView?.indexPathForItem(at: touchPoint)
        //print("\(indexPath!.row)")
        
        let gestureFlickrService = FlickrService.sharedInstance
        let favoris = gestureFlickrService.addPictureFavoris(index: indexPath!.row)
        
        let favSplit = favoris.components(separatedBy: "@")
        
        // UIAlertController (button OK)
        let alertController = UIAlertController(title: "Ajouter aux favoris", message: "Insertion réussie", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            // Insertion en CoreData
            self.save(titre:favSplit[1], url:favSplit[0])
            print("Successfully inserting")
        }
        alertController.addAction(OKAction)
        present(alertController, animated: true)
        
        // Mise en dossier
        // Donne un tableau
        if let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            // Path du dossier a ecrire
            let imagePath = documentDirectoryURL.appendingPathComponent(favSplit[1])
            print(favSplit[1])
            // Telechargement de l'image de maniere synchrone
            if let imageURL = URL(string: favSplit[0]),
                let data = try? Data(contentsOf: imageURL),
                let image = UIImage(data: data){
                
                do {
                    try UIImageJPEGRepresentation(image, 1.0)?.write(to: imagePath)
                    print("Insertion dans le dossier reussi")
                } catch {
                    fatalError("Erreur d'ecriture")
                }
                
            }
            
        }

    }
    
    // Insertion en base CoreData
    func save(titre: String, url: String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Favoris", in: managedContext)!
        let pictureFlickr = NSManagedObject(entity: entity, insertInto: managedContext)
        
        pictureFlickr.setValue(titre, forKey: "titre")
        pictureFlickr.setValue(url, forKey: "url")
        
        do {
            try managedContext.save()
            print("Insert Data")
        } catch let error as NSError {
            print("Crash \(error)")
        }
    }
    
    
    
}













