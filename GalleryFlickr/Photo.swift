//
//  Photo.swift
//  GalleryFlickr
//
//  Created by Apple on 30/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

struct Photo {
    
    var id = String()
    var secret = String()
    var server = String()
    var farm = String()
    //var titre: [String] = []
    
    var urlFlickr: [String] = []
    
    var apiKey = String()

    
}
