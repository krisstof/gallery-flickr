//
//  DetailFlickrViewController.swift
//  GalleryFlickr
//
//  Created by Apple on 29/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import CoreData

class DetailFlickrViewController: UIViewController {

    var image: UIImage?
    var titre: String?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = image
        labelTitle.text = titre
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func longPressSaveInCameroRoll(_ sender: Any) {
       
        // UIAlertController (button OK)
        let alertController = UIAlertController(title: "Insertion dans pellicule", message: "Photo insérée", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            
            let imageData = UIImagePNGRepresentation(self.imageView.image!)
            let compressedImage = UIImage(data: imageData!)
            UIImageWriteToSavedPhotosAlbum(compressedImage!, nil, nil, nil)
            
            print("Successfully inserting")
        }
        alertController.addAction(OKAction)
        present(alertController, animated: true)
        
    }
    

    
    

}
