//
//  ViewController.swift
//  GalleryFlickr
//
//  Created by Apple on 24/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var searchTherme: UITextField!
    @IBOutlet weak var goSearch: UIButton!
    
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var goSearchText: UIButton!
    
    @IBOutlet weak var searchTakenDate: UITextField!
    @IBOutlet weak var goSearchTakenDate: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    // fermer le clavier a appeler dans le segue pour fermer la vue
    func textFieldShouldReturntext(Field: UITextField) -> Bool {
        for textField in self.view.subviews where textField is UITextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "therme-api"{
            if let searchApiController = segue.destination as? GalleryJsonCollectionViewController {
                
                searchApiController.flickrTags = self.searchTherme.text!
                searchApiController.flickrText = self.searchText.text!
                searchApiController.flickrTaken = self.searchTakenDate.text!
                
                textFieldShouldReturntext(Field: searchTherme)
            }
        }
    }
    
    
}

