//
//  FavorisCollectionViewController.swift
//  GalleryFlickr
//
//  Created by Apple on 05/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "Cell"

class FavorisCollectionViewController: UICollectionViewController {

    var tabUrlFavorisUrl: [String] = []
    var tabUrlFavorisName: [String] = []
    
    let tap = UITapGestureRecognizer()
    
    //let imageName = "my-image"

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    func DeleteCoreData(sender: AnyObject?) {
        print("Single Tap!")
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.getData()
        self.collectionView?.reloadData()
        
    }
    
    // Recuperation des donnes
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func getData() {
        
        // On remet les tableaux a vide
        tabUrlFavorisUrl = []
        tabUrlFavorisName = []
        
        let fetchRequest: NSFetchRequest<Favoris> = Favoris.fetchRequest()
        // Lecture des données
        do {
            let searchResults = try getContext().fetch(fetchRequest)
            
            // Conversion NSManagedObject pour utiliser 'for'
            for trans in searchResults as [NSManagedObject] {
                let appUrl = trans.value(forKey: "url")
                tabUrlFavorisUrl.append(appUrl as! String)
                
                let appTitre = trans.value(forKey: "titre")
                tabUrlFavorisName.append(appTitre as! String)
            }
        } catch {
            print("Error with request: \(error)")
        }
        //print(tabUrlFavorisName)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "image-favoris-segue"{
            if let cell = sender as? FavorisCollectionViewCell,
                let detailImageFavoris = segue.destination as? DetailFlickrViewController {
                
                detailImageFavoris.image = cell.cellImage.image
                detailImageFavoris.titre = cell.labelFavoris.text!
            }
        }
    }
    

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return tabUrlFavorisUrl.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FavorisCollectionViewCell
    
        // On vide les cellules
        cell.cellImage.image = nil
        let favorisName = tabUrlFavorisName[indexPath.row]
        
        if let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            // Path du dossier a lire
            let imagePath = documentDirectoryURL.appendingPathComponent(favorisName)
            print(imagePath.path)
            
            // Lecture des image (elle non pas d'extension)
            guard let loadedImage = UIImage(contentsOfFile: imagePath.path) else {
                fatalError("Erreur de lecture")
            }
            
            DispatchQueue.main.async {
                cell.cellImage.image = loadedImage
                cell.labelFavoris.text = favorisName
            }
            
            
        }
        
        // Appuie long sur l'image pour lancer la fonction de suppression
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longTapFuncfavoris))
        cell.addGestureRecognizer(longPressRecognizer)
        
        return cell
    }

    func longTapFuncfavoris(sender: UILongPressGestureRecognizer) {
        
        // Recuperation de l'index de l'image
        let touchPoint = sender.location(in: collectionView)
        let indexPath = collectionView?.indexPathForItem(at: touchPoint)
        
        let supPictureFavoris = indexPath!.row
        //print(supPictureFavoris)
        
        // UIAlertController (button OK)
        let alertController = UIAlertController(title: "Suppression des Favoris", message: "Image supprimée", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            //self.deletePictureCoreData(id: supPictureFavoris)
            print("Suppression OK")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            print("Supp Cancel")
        }
        alertController.addAction(cancelAction)
        alertController.addAction(OKAction)
        present(alertController, animated: true)
        
    }
    
    
    
    func deletePictureCoreData(url: String) {
        
        guard let appDel:AppDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
         
        
    }

}
