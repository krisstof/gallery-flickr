//
//  MapViewController.swift
//  GalleryFlickr
//
//  Created by Apple on 11/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var viewMap: MKMapView!
    
    var mapPicture: [[String:Double]] = []
    var mapLatitude: [Double] = []
    var mapLongitude: [Double] = []
    let sophia = CLLocationCoordinate2D(latitude: 43.6163539, longitude: 7.05522210000000027)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // SharedInstance
        
        //print(mapPicture["latitude"])
        //print(mapPicture[0])
        //let mapSplit = mapPicture[0].components(separatedBy: "@")
        
        //print(mapSplit[0]+" : "+mapSplit[1])
        //mapLatitude = Double(mapSplit[0])!
        //mapLongitude = mapSplit[1]
        //let sophia = CLLocationCoordinate2D(latitude: mapSplit[0], longitude: mapSplit[1])
        
        viewMap.showsScale = true
        // Affiche le compas (boussole)
        viewMap.showsCompass = true
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = sophia
        annotation.title = "Sophia"
        annotation.subtitle = "Antipolis"
        
        viewMap.addAnnotation(annotation)
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let mapFlickrService = FlickrService.sharedInstance
        //print(mapFlickrService.urlFlickrLoc)
        
        mapPicture = mapFlickrService.urlFlickrLoc
        /*for index in 1...mapPicture.count {
            print(mapPicture[index]["latitude"]!)
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    

}
